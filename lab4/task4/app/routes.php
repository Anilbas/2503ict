<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', function()
{
	return View::make('socialnet.home');
});

Route::get('friends', function()
{
	return View::make('socialnet.friends');
});

Route::get('message', function()
{
	return View::make('socialnet.message');
});

Route::get('profile', function()
{
	return View::make('socialnet.profile');
});

Route::get('logout', function()
{
	return View::make('socialnet.logout');
});