<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
require "models/users.php";


// Display search form
Route::get('/', function()
{
	return View::make('users.query');
});

// Perform search and display results
Route::get('search', function()
{
 // $name = Input::get('name');
 // $year = Input::get('year');
 // $state = Input::get('state');
  $input = Input::get('input');
  
  $results = search($input);

	return View::make('users.results')->withUsers($results)->withInput($input);
});


/* Functions for Library users database example. */

/* Search sample data for $input from form. */
function search($input) {
  $users = getUsers();

 /* // Filter $pms by $name
  if (!empty($name)) {
    $results = array();
    foreach ($pms as $pm) {
      if (stripos($pm['name'], $name) !== FALSE) {
        $results[] = $pm;
      }
    }
    $pms = $results;
  }

  // Filter $pms by $year
  if (!empty($year)) {
    $results = array();
    foreach ($pms as $pm) {
      if (strpos($pm['from'], $year) !== FALSE || 
          strpos($pm['to'], $year) !== FALSE) {
        $results[] = $pm;
      }
    }
    $pms = $results;
  }

  // Filter $pms by $state
  if (!empty($state)) {
    $results = array();
    foreach ($pms as $pm) {
      if (stripos($pm['state'], $state) !== FALSE) {
        $results[] = $pm;
      }
    }
    $pms = $results;
  }*/
  

  if (!empty($input)) {
    $results = array();
    foreach ($users as $user) {
      if (stripos($user['name'], $input) !== FALSE || stripos($user['address'], $input) !== FALSE ||
            strpos($user['phone'], $input)   !== FALSE || strpos($user['email'], $input)      !== FALSE) {
	     	$results[] = $user;
	    }
	  }
	  $users = $results;
  } 

  return $users;
}