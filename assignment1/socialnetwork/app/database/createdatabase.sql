PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
DROP TABLE "migrations";
DROP TABLE "posts";
DROP TABLE "comments";
CREATE TABLE "migrations" ("migration" varchar not null, "batch" integer not null);
INSERT INTO "migrations" VALUES('2016_04_16_105253_create_posts_table',1);
INSERT INTO "migrations" VALUES('2016_04_23_105253_create_posts_and_comments',2);
INSERT INTO "migrations" VALUES('2016_04_22_105253_create_posts_and_comments',3);
INSERT INTO "migrations" VALUES('2016_04_21_105253_create_posts_and_comments',4);
CREATE TABLE "posts" ("id" integer not null primary key autoincrement, "user" varchar not null, "title" varchar not null, "message" text not null, "icon_addr" varchar not null, "created_at" datetime not null, "updated_at" datetime not null);
INSERT INTO "posts" VALUES(9,'user1','First post','There is no one who loves pain itself, who seeks after it and wants to have it, simply because it is pain...','search.png','2016-04-24 12:45:49','2016-04-24 12:45:49');
INSERT INTO "posts" VALUES(11,'user2','Second post','Neque porro quisquam est qui dolorem ipsum quia dolor sit amet, consectetur, adipisci velit...','search.png','2016-04-24 12:50:00','2016-04-24 12:50:00');
INSERT INTO "posts" VALUES(12,'user1','Third post','Lorem ipsum dolor sit amet, consectetur adipiscing elit. <b>Lorem</b> Integer nec odio. Praesent <b>Lorem</b> libero. <b>Lorem</b> Sed cursus ante dapibus diam. Sed nisi. Nulla quis sem at nib.','search.png','2016-04-24 12:50:19','2016-04-24 12:50:19');
CREATE TABLE "comments" ("id" integer not null primary key autoincrement, "post_id" integer not null, "user" varchar not null, "message" text not null, "created_at" datetime not null, "updated_at" datetime not null);
INSERT INTO "comments" VALUES(12,9,'user2','cool post!','2016-04-24 12:46:19','2016-04-24 12:46:19');
INSERT INTO "comments" VALUES(13,9,'user3','hi','2016-04-24 12:46:41','2016-04-24 12:46:41');
INSERT INTO "comments" VALUES(14,11,'user3','hi','2016-04-24 12:53:55','2016-04-24 12:53:55');
DELETE FROM sqlite_sequence;
INSERT INTO "sqlite_sequence" VALUES('posts',20);
INSERT INTO "sqlite_sequence" VALUES('comments',19);
COMMIT;
