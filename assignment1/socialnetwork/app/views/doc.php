<!DOCTYPE html>
<html>
<head>
    <title>Documentation</title>
</head>

<body>
<h1>Documentation</h1>
<a href="./homepage">Back to homepage</a> <br>
Entity-relationship diagram for the database: <br>
<img src="ERD.png"  alt="ERD" height="300"> <br> <br>
I was able to complete all the main specifications of the assignment, namely:<br>
1. All pages must have a navigation menu, either across the top of the page or down the left or right column. <br>
2. The home page must display a form for the user to create a new post. <br>
3. The home page must also display all posts, in reverse-order of creation (most recent posts first). <br>
4. Each post should contain an icon (all posts can have the same icon). <br>
5. Each post should contain a title, a message, and a user&rsquo;s name (the user is not required to login, they can simply enter their name in the post form). <br>
6. Each post should indicate the number of comments. <br>
7. Each post should have edit and delete buttons or links. <br>
8. Each post should have a view comments link. <br>
9. When the edit button is pressed an edit page should be displayed which presents a form to edit the contents of the post. A cancel button on this page simply redirects back to the home page without any changes. A save button, updates the post and redirects to the view comments page. <br>
10. When the view comments link is pressed, a new page (the view comments page) is displayed showing the post as well as this post&rsquo;s comments. <br>
11. The view comments page should contain a form to add a new comment to the post. <br>
12. Comments have a message and a user, but no title. <br>
13. Comments have a delete button but no edit button. Deleting a comment should redirect to the view comments page. <br>
14. This assignment must be implemented using Laravel and the DB class. <br>
15. SQL sanitisation - All SQL values are sanitised.<br>
16. HTML sanitisation - All HTML output is sanitised.<br>
17. HTML validated using W3C validator. <br>
<br>
I was not able to complete:<br>
1. Input validation and error display in the form. <br>
2. Support of multiple paragraphs in the post. <br>
<a href="./homepage">Back to homepage</a> <br>
</html>



