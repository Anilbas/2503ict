@extends('layouts.scaffold')

@section('content')
              <div class='list-group-item'>
              <h3>{{{ $post->title }}}</h3>
              <img class='photo' src='{{{ url("$post->icon_addr") }}}' alt='photo'> <br>
              Author: {{{ $post->user }}} <br>
              Message: {{{ $post->message }}} <br>
              Created: {{{ $post->created_at }}} <br>
              Updated: {{{ $post->updated_at }}} <br>
              </div>
@if ($comments)
Comments <br>
@foreach ($comments as $comment)
              <div class='list-group-item'>
              Author: {{{ $comment->user }}} <br>
              Message: {{{ $comment->message }}} <br>
              Created: {{{ $comment->created_at }}} <br>
              <a href='{{{ url("delete_comment_action-$post->id-$comment->id") }}}'>Delete</a> 
              </div>
@endforeach
@else
<p>No comments found.</p> 
@endif

<form method="post" action='{{{ url("add_comment_action-$post->id") }}}'>
<!--    Add comment -->
    <table class='list-group-item'>
    <tr><td>Your name:</td> <td><input type="text" name="user"></td></tr>
    <tr><td>Message:</td> <td><textarea name="message"></textarea></td></tr>
    <tr><td colspan=2><input type="submit" value="Submit"></td></tr>
    </table>
</form> 
@stop
              