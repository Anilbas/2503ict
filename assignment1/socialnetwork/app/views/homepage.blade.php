@extends('layouts.scaffold')


@section('content')
<form method="post" action="add_post_action">
    <table class='list-group-item'>
    <tr><td>Your name:</td> <td><input type="text" name="user" required></td></tr>
    <tr><td>Title:</td> <td><input type="text" name="title"></td></tr>
    <tr><td>Message:</td> <td><textarea name="message" required></textarea></td></tr>
    <tr><td colspan=2><input type="submit" value="Submit"></td></tr>
    </table>
</form> 


@if ($posts)
@foreach ($posts as $post)
              <div class='list-group-item'>
              <h3>{{{ $post->title }}}</h3>
              <img class='photo' src='{{{ url("$post->icon_addr") }}}' alt='photo'> <br>
              Author: {{{ $post->user }}} <br>
              Message: {{{ $post->message }}} <br>
              Created: {{{ $post->created_at }}} <br>
              Updated: {{{ $post->updated_at }}} <br>
              <a href='{{{ url("update_post-$post->id") }}}'>Edit </a>
              <a href='{{{ url("delete_post_action-$post->id") }}}'>Delete</a>
              <a href='{{{ url("view_comments-$post->id") }}}'>View comments</a> 
              <?php $zerocomments="true"; ?>
              @foreach ($comments as $comment)
              @if ($comment->id==$post->id)
              {{{ $comment->count}}} 
              @if ($comment->count==1)
              comment.
              @else
              comments.    
              @endif
              <?php $zerocomments="false"; ?>             
              @endif
              @endforeach
              @if ($zerocomments=="true")
              0 comments.
              @endif 
              </div>
@endforeach
@else
<p>No posts found.</p>
@endif
@stop