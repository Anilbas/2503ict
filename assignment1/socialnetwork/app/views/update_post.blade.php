@extends('layouts.scaffold')

@section('content')
<form method="post" action="{{{ url('update_post_action') }}}">
  <input type="hidden" name="id" value="{{{ $post->id }}}"> 
  <table>
    <tr><td>Title:</td> <td><input type="text" name="title" value="{{{ $post->title }}}"></td></tr>
    <tr><td>Message:</td> <td><textarea name="message">{{{ $post->message }}}</textarea></td></tr>
    <tr><td><input type="submit" value="Save"></td> <td> <a href='./homepage'> Cancel </a> </td></tr>
  </table>
</form> 
@stop