<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/*Route::get('/', function()
{
	return View::make('hello');
});*/

//Root route - redirects to /homepage
Route::get('/', function()
{
    return Redirect::to(url("homepage"));
});

//Documentation page route
Route::get('/doc', function()
{
    return View::make('doc');
});

//Homepage with all posts and new post form. The route calls 2 functions to perform SQL queries to get all post info and comment counts
Route::get('homepage', function()
{
  $posts = get_posts();
  $comments=get_comment_counts();
  return View::make('homepage')->withPosts($posts)->withComments($comments);
});

//Shows comments page with comments for the post with the given ID
Route::get('view_comments-{id}', function ($id)
{
  $post = get_post($id);
  $comments = get_comments($id);
	return View::make('view_comments')->withPost($post)->withComments($comments);
}); 

//Called by the Submit button on the homepage, adds a post with info from the form (using a function); redirects to homepage. 
Route::post('add_post_action', function()
{
  $title = Input::get('title');
  $message = Input::get('message');
  $user = Input::get('user');

  $id = add_post($title, $message, $user);

  // If successfully created then display newly created post
  if ($id) 
  {
       return Redirect::to(url("homepage"));
  } 
  else
  {
    die("Error adding post");
  }
});

//Called from the Comments page, by the new comment form. On pressing of Submit button, 
//adds the new comment to the database, and redirects to the same page.
Route::post('add_comment_action-{id}', function($id)
{
  $message = Input::get('message');
  $user = Input::get('user');

  $idc = add_comment($id, $message, $user);

  // If successfully created then display newly created post
  if ($idc) 
  {
       return Redirect::to(url("view_comments-$id"));
  } 
  else
  {
    die("Error adding comment");
  }
});

//Route to the page with the Update post form. Shows form to update post with the given ID.
Route::get('update_post-{id}', function($id)
{
  $post = get_post($id);
  return View::make('update_post')->withPost($post);
});

//Called by the Submit button in the Update post form; updates the post in the database and redirects to Comments page for this post
Route::post('update_post_action', function()
{
  $title = Input::get('title');
  $message = Input::get('message');
  $id = Input::get('id');

  $id = update_post($id, $title, $message);

  // If successfully updated then display 
  if ($id) 
  {
     return Redirect::to(url("view_comments-$id"));
  } 
  else
  {
    die("Error updating post");
  }
});

//Called by the Delete link on the post. Deletes the post and redirects back to the Homepage
Route::get('delete_post_action-{id}', function($id)
{
  $sql = "delete from posts where id = ?";
  DB::delete ($sql, array($id));
  
    $sql = "delete from comments where post_id = ?";
  DB::delete ($sql, array($id));
  
  return Redirect::to(url("homepage"));
});

//Called by the Delete link on a comment. Deletes the comment and redirects to the Comments page for the same post.
Route::get('delete_comment_action-{postid}-{comid}', function($postid, $comid)
{
  $sql = "delete from comments where id = ?";
  DB::delete ($sql, array($comid));
  
  return Redirect::to(url("view_comments-$postid"));
});

//Gets all the post information from the database
function get_posts()
{
  $sql = "select * from posts order by created_at desc";
  $posts = DB::select($sql);
  return $posts;
}

//Gets number of comments for every post with comments
function get_comment_counts()
{
  $sql = "select post_id as id, count(*) as count from comments group by post_id order by post_id";
  $posts = DB::select($sql);
  return $posts;
}

//Gets all the comment information for the particular post
function get_comments($id)
{
  $sql = "select * from comments where post_id = ? order by created_at desc";
  $posts = DB::select($sql, array($id));

  return $posts;
}

/* Gets post with the given id */
function get_post($id)
{
	$sql = "select * from posts where id = ?";
	$posts = DB::select($sql, array($id));

	// If we get more than one post or no posts display an error
	if (count($posts) != 1) 
	{
    die("Invalid query or result: $query\n");
  }

	// Extract the first post (which should be the only post)
  $post = $posts[0];
	return $post;
}

//Adds a post to the database with form information
function add_post($title, $message, $user) 
{
  $sql = "insert into posts (title, message, user, icon_addr, created_at, updated_at) values ( ?, ?, ?, \"search.png\", datetime('now'), datetime('now'))";

  DB::insert($sql, array($title, $message, $user));

  $id = DB::getPdo()->lastInsertId();

  return $id;
}

//Adds a comment to the database with form information
function add_comment($id, $message, $user)
{
  $sql = "insert into comments (post_id, message, user, created_at, updated_at) values (?, ?, ?, datetime('now'), datetime('now'))";

  DB::insert($sql, array($id, $message, $user));

  $id = DB::getPdo()->lastInsertId();

  return $id;
}

//Updates the post in the database with the form information.
function update_post($id, $title, $message) 
{
  $sql = "update posts set title = ?, message = ?, updated_at = datetime('now') where id = ?";
  DB::update($sql, array($title, $message, $id));

//  $id = DB::getPdo()->lastInsertId();

  return $id;
}