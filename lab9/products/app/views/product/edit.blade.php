@extends ('product.layout')

@section ('body')
     {{{ $product->name }}} edit <br>
     {{ Form::model($product, array('method' => 'PUT', 'route' =>
array('product.update', $product->id)));  }}
     {{ Form::label('name', 'Product Name: ') }}
     {{ Form::text('name') }} 
     <font color="red">{{ $errors->first('name') }}</font><br>
     {{ Form::label('price', 'Price: ') }}
     {{ Form::text('price') }} 
     <font color="red">{{ $errors->first('price') }}</font><br>
     {{ Form::submit('Update') }}
     {{ Form::close(); }}
     

@stop