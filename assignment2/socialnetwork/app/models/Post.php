<?php

class Post extends Eloquent {
     public static $rules = array(
        'title' => 'required',
        'message' => 'required|min:3'
        );
  
    function comments()
    {
      return $this->hasMany('Comment');
    }

   /* public function user(){
     return $this->belongsTo("User");
    }*/
}
