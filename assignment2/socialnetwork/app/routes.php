<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::get('/', array('as'=>'root_of_project', 'uses'=> 'PostController@index')); 

Route::get('/home', array('as'=>'home', 'uses'=> 'PostController@index')); 

//Documentation page route
Route::get('/doc', array('as' => 'doc', function()
{
    return View::make('doc');
}));

//Called from the Comments page, by the new comment form. On pressing of Submit button, 
//adds the new comment to the database, and redirects to the same page.
Route::post('add_comment_action-{id}', function($id)
{
  $message = Input::get('message');
  $user = Input::get('user');

  $idc = add_comment($id, $message, $user);

  // If successfully created then display newly created post
  if ($idc) 
  {
       return Redirect::route('post.show', array($id));
  } 
  else
  {
    die("Error adding comment");
  }
});

//Called by the Delete link on a comment. Deletes the comment and redirects to the Comments page for the same post.
Route::get('delete_comment_action-{postid}-{comid}', function($postid, $comid)
{
  $sql = "delete from comments where id = ?";
  DB::delete ($sql, array($comid));
  return Redirect::route('post.show', array($postid));
});

//Gets number of comments for every post with comments
function get_comment_counts()
{
  $sql = "select post_id as id, count(*) as count from comments group by post_id order by post_id";
  $posts = DB::select($sql);
  return $posts;
}

//Adds a comment to the database with form information
function add_comment($id, $message, $user)
{
  $sql = "insert into comments (post_id, message, user, created_at, updated_at) values (?, ?, ?, datetime('now'), datetime('now'))";

  DB::insert($sql, array($id, $message, $user));

  $id = DB::getPdo()->lastInsertId();

  return $id;
}

Route::resource ('post', 'PostController');

Route::post('user/login', array('as' => 'user.login', 'uses' => 'UserController@login'));

Route::get('user/logout', array('as' => 'user.logout', 'uses' => 'UserController@logout')); 

Route::get('user/follow', array('as' => 'user.follow', 'uses' => 'UserController@follow'));

Route::get('user/unfollow', array('as' => 'user.unfollow', 'uses' => 'UserController@unfollow')); 

Route::get('user/showfriends/{user}', array('as' => 'user.showfriends', 'uses' => 'UserController@showfriends')); 

Route::resource ('user', 'UserController');