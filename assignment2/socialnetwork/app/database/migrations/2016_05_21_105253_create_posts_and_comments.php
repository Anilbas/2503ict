<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreatePostsAndComments extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('posts', function(Blueprint $table) {
			$table->increments('id');
			$table->string('user');
			$table->string('title');
			$table->text('message')->nullable();
			$table->string('privacy');			
			$table->timestamps();
		});

		Schema::create('comments', function(Blueprint $table) {
			$table->increments('id');
			$table->string('post_id');
			$table->string('user');
			$table->text('message');
			$table->timestamps();
		});
		
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{

		Schema::dropIfExists('comments');		
		Schema::dropIfExists('posts');

	}

}

