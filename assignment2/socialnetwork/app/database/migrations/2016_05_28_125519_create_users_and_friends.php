<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersAndFriends extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('users', function(Blueprint $table) {
		    $table->increments('id');
            $table->string('username')->unique();
            $table->string('password')->index();
            $table->string('full_name')->nullable();
            $table->date('date_of_birth')->nullable();  
            $table->string('remember_token')->nullable();
            $table->timestamps();
		});
		
		Schema::create('friends', function(Blueprint $table) {
		    $table->increments('id');
		    $table->string('follower');
			$table->string('following');
			$table->timestamps();
		});
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::dropIfExists('friends');
		Schema::dropIfExists('users');
	}

}
