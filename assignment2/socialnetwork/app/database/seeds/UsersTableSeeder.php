<?php

class UsersTableSeeder extends Seeder {

	public function run()
	{
		// wipe the table clean before populating
    		DB::table('users')->truncate();

		
	      	$user = new User;
        $user->username = "Bob@a.org";
        $user->password =  Hash::make("1234");
        $user->full_name = "Bob";
        $user->date_of_birth = date("1970-01-02");
        $user->save();
        
       	$user = new User;
        $user->username = 'John@a.org';
        $user->password = Hash::make('1234');
        $user->date_of_birth = date("1970-01-03");
        $user->full_name = "John";
        $user->save();
        
        $user = new User;
        $user->username = 'Tom@a.org';
        $user->password = Hash::make('1234');
        $user->date_of_birth = date("1970-01-04");
        $user->full_name = "Tom";
        $user->save();
        
        $user = new User;
        $user->username = "Jane@a.org";
        $user->password =  Hash::make("1234");
        $user->full_name = "Jane";
        $user->date_of_birth = date("1970-01-05");
        $user->save();
        
       	$user = new User;
        $user->username = 'Kate@a.org';
        $user->password = Hash::make('1234');
        $user->date_of_birth = date("1970-01-06");
        $user->full_name = "Kate";
        $user->save();
        
        $user = new User;
        $user->username = 'Ann@a.org';
        $user->password = Hash::make('1234');
        $user->date_of_birth = date("1970-01-07");
        $user->full_name = "Ann";
        $user->save();
        
        $user = new User;
        $user->username = "Sam@a.org";
        $user->password =  Hash::make("1234");
        $user->full_name = "Sam";
        $user->date_of_birth = date("1970-01-08");
        $user->save();
        
       	$user = new User;
        $user->username = 'Pat@a.org';
        $user->password = Hash::make('1234');
        $user->date_of_birth = date("1970-01-09");
        $user->full_name = "Pat";
        $user->save();
        
        $user = new User;
        $user->username = 'Alex@a.org';
        $user->password = Hash::make('1234');
        $user->date_of_birth = date("1970-01-10");
        $user->full_name = "Alex";
        $user->save();
        
        $user = new User;
        $user->username = 'Casey@a.org';
        $user->password = Hash::make('1234');
        $user->date_of_birth = date("1970-01-10");
        $user->full_name = "Casey";
        $user->save();
     

	}

}
