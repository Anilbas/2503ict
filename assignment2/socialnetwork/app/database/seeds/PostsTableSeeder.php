<?php

class PostsTableSeeder extends Seeder {

	public function run()
	{
	    DB::table('posts')->truncate();
        
        $post = new Post;
        $post->user = 'John@a.org';
        $post->title = 'John\'s Post 1';
        $post->message = 'John\'s public post';
        $post->privacy = 'public';
        $post->save();
        
        $post = new Post;
        $post->user = 'John@a.org';
        $post->title = 'John\'s Post 2';
        $post->message = 'John\'s friends post';
        $post->privacy = 'friends';
        $post->save();
        
        $post = new Post;
        $post->user = 'John@a.org';
        $post->title = 'John\'s Post 3';
        $post->message = 'John\'s private post';
        $post->privacy = 'private';
        $post->save();
        
        $post = new Post;
        $post->user = 'John@a.org';
        $post->title = 'John\'s Post 4';
        $post->message = 'John\'s public post';
        $post->privacy = 'public';
        $post->save();
        
        $post = new Post;
        $post->user = 'John@a.org';
        $post->title = 'John\'s Post 5';
        $post->message = 'John\'s friends post';
        $post->privacy = 'friends';
        $post->save();
        
        $post = new Post;
        $post->user = 'John@a.org';
        $post->title = 'John\'s Post 6';
        $post->message = 'John\'s private post';
        $post->privacy = 'private';
        $post->save();
        
        $post = new Post;
        $post->user = 'John@a.org';
        $post->title = 'John\'s Post 7';
        $post->message = 'John\'s public post';
        $post->privacy = 'public';
        $post->save();
        
        $post = new Post;
        $post->user = 'John@a.org';
        $post->title = 'John\'s Post 8';
        $post->message = 'John\'s friends post';
        $post->privacy = 'friends';
        $post->save();
        
        $post = new Post;
        $post->user = 'John@a.org';
        $post->title = 'John\'s Post 9';
        $post->message = 'John\'s private post';
        $post->privacy = 'private';
        $post->save();
        
        $post = new Post;
        $post->user = 'John@a.org';
        $post->title = 'John\'s Post 10';
        $post->message = 'John\'s public post';
        $post->privacy = 'public';
        $post->save();
        
        $post = new Post;
        $post->user = 'Bob@a.org';
        $post->title = 'Bob\'s Post 1';
        $post->message = 'Bob\'s public post';
        $post->privacy = 'public';
        $post->save();
        
        $post = new Post;
        $post->user = 'Bob@a.org';
        $post->title = 'Bob\'s Post 2';
        $post->message = 'Bob\'s friends post';
        $post->privacy = 'friends';
        $post->save();
        
        $post = new Post;
        $post->user = 'Bob@a.org';
        $post->title = 'Bob\'s Post 3';
        $post->message = 'Bob\'s private post';
        $post->privacy = 'private';
        $post->save();

	}
}