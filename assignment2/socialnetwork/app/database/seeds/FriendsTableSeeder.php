<?php

class FriendsTableSeeder extends Seeder {

	public function run()
	{
		// wipe the table clean before populating
		   DB::table('friends')->truncate();		

		
	    $friend = new Friend;
        $friend->follower = "Bob@a.org";
        $friend->following =  "John@a.org";
        $friend->save();
        
        $friend = new Friend;
        $friend->follower = "John@a.org";
        $friend->following = "Bob@a.org";
        $friend->save();
        
        $friend = new Friend;
        $friend->follower = "Jane@a.org";
        $friend->following = "Kate@a.org";
        $friend->save();
        
        $friend = new Friend;
        $friend->follower = "Kate@a.org";
        $friend->following = "Jane@a.org";
        $friend->save();
        
        $friend = new Friend;
        $friend->follower = "Jane@a.org";
        $friend->following = "Ann@a.org";
        $friend->save();
        
        $friend = new Friend;
        $friend->follower = "Ann@a.org";
        $friend->following = "Jane@a.org";
        $friend->save();
    
        
	}

}