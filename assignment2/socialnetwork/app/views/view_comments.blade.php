@extends('layouts.scaffold')

@section('content')
              <div class='list-group-item'>
              <h4>{{{ $post->title }}}</h4>
              Author: {{{ $post->user }}} <br>
              Message: {{{ $post->message }}} <br>
              Created: {{{ $post->created_at }}} <br>
              Updated: {{{ $post->updated_at }}} <br>
              Privacy: {{{ $post->privacy }}} <br>
              </div>
@if ($comments)
<h4>Comments</h4>
@foreach ($comments as $comment)
              <div class='list-group-item'>
              Author: {{{ $comment->user }}} <br>
              Message: {{{ $comment->message }}} <br>
              Created: {{{ $comment->created_at }}} <br>
            @if (Auth::check() && $comment->user == Auth::user()->username)
              <a href='{{{ url("delete_comment_action-$post->id-$comment->id") }}}'>Delete</a> 
            @endif
              </div>
@endforeach
@else
<p>No comments found.</p> 
@endif

@if (Auth::check())

 <div class='list-group-item'>
     {{ Form::open(array('url' => array("add_comment_action-$post->id")));  }}
     {{ Form::hidden('user', Auth::user()->username) }}
     {{ Form::label('message', 'Message: ') }}
     {{ Form::textarea('message', null, ['size' => '60x3']) }} <br>
     {{ Form::submit('Submit') }} 
     {{ Form::close(); }} 
 </div> 
@endif

@stop
              