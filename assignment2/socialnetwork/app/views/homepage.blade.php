@extends('layouts.scaffold')


@section('content') 
  @if (Auth::check())
 <div class='list-group-item'>
     {{ Form::open(array('route' => array('post.store')));  }}
     {{ Form::hidden('user', Auth::user()->username) }}
     {{ Form::label('title', 'Title: ') }}
     {{ Form::text('title') }} 
     <font color="red">{{ $errors->first('title') }}</font>  <br>
     {{ Form::label('message', 'Message: ') }}
     {{ Form::textarea('message', null, ['size' => '60x3']) }} <br>
     <font color="red">{{ $errors->first('message') }}</font>  <br>
     {{ Form::select('privacy', array('public' => 'public', 'friends' => 'friends', 'private' => 'private'), 'friends'); }} <br> <br>
     {{ Form::submit('Submit') }} 
     {{ Form::close(); }} 
 </div>  
 @endif


@if ($posts)
@foreach ($posts as $post)
 <?php   
       $temp = array_where($friends, function($key, $value) use ($post)
       {
           return ($value->following == $post->user );
       }); 
       $isfriend = count($temp);
      ?>
{{-- public post or own post or user is a friend --}}
 @if ($post->privacy == "public" || (Auth::check() && Auth::user()->username == $post->user) || ($isfriend && $post->privacy == "friends"))
              <div class='list-group-item'>
              <h4>{{{ $post->title }}}</h4>
              Author: {{{ $post->user }}} <br>
              Message: {{{ $post->message }}} <br>
              Created: {{{ $post->created_at }}} <br>
              Updated: {{{ $post->updated_at }}} <br>
              Privacy: {{{ $post->privacy }}} <br>
              <?php $zerocomments="true"; ?>
              @foreach ($comments as $comment)
                @if ($comment->id==$post->id)
                {{{ $comment->count}}} 
                  @if ($comment->count==1)
                    comment.
                  @else
                    comments.    
                  @endif
                 <?php $zerocomments="false"; ?>             
                @endif
              @endforeach
              @if ($zerocomments=="true")
                0 comments.
              @endif 
              {{ link_to_route ('post.show', 'View comments', array($post->id)) }}
              {{-- own posts - can edit and delete --}}
              @if (Auth::check() && $post->user == Auth::user()->username)
              <br>{{ link_to_route ('post.edit', 'Edit', array($post->id)) }} 
              {{ Form::open(array('method' => 'DELETE', 'route' => array('post.destroy', $post->id))) }}
                  {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }} 
              {{ Form::close() }} 
             @endif
              </div>
 @endif
@endforeach
@else
<p>No posts found.</p>
@endif
@stop