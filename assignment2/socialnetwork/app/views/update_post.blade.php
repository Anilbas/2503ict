@extends('layouts.scaffold')

@section('content')
     {{ Form::model($post, array('method' => 'PUT', 'route' => array('post.update', $post->id)));  }}
     {{ Form::label('title', 'Title: ') }}
     {{ Form::text('title') }} 
     <font color="red">{{ $errors->first('title') }}</font>  <br>
     {{ Form::label('message', 'Message: ') }}
     {{ Form::textarea('message', null, ['size' => '60x3']) }} <br>
     <font color="red">{{ $errors->first('message') }}</font>  <br>
     {{ Form::select('privacy', array('public' => 'public', 'friends' => 'friends', 'private' => 'private'), 'friends'); }} <br>
     {{ Form::submit('Update') }} {{ link_to_route ('home', 'Cancel')}}
     {{ Form::close(); }}
@stop