<!DOCTYPE html>
<html>
<head>
    <title>Documentation</title>
</head>

<body>
<h1>Documentation</h1>
<a href="./home">Back to homepage</a> <br>
Entity-relationship diagram for the database: <br>
<img src="ERD.png"  alt="ERD" height="500"> <br> <br>
<b>I was able to complete the following:</b><br>
Task 1 &ndash; Re-implement Assignment 1<br>
Implement the functionalities from Assignment 1 to use migrations, seeders, models, controllers, and the Validator class.<br>
Task 2 &ndash; Add new functionality<br>
A user must have the following attributes:<br>
&bull; Email (which will also be their username)<br>
&bull; Password<br>
&bull; Fullname<br>
&bull; Date of birth<br>
&bull; Profile image<br>
Users of the website can search for other users without being logged in. When selecting a user the<br>
profile page for the user will be shown <br>
The profile page will only show public posts from that user if no user is currently logged in. If the user<br>
is logged in and is a friend then posts visible to friends will also be shown and also a remove friend<br>
button should be displayed to allow removal of this friend. If the logged in user is not a friend, then an<br>
add friend button will be displayed to add this user as a friend.<br>
If the user is not logged in, they cannot comment on any posts or make any posts.<br>
A user can create a new account through a link in the navigation menu. When creating an<br>
account, user can upload a profile image. This image will be displayed in user profile and user search results.<br>
A user may log in using a login form in the navigation menu. The user must login using their<br>
email address and password. When logged in a user will be able to perform the following:<br>
&bull; Add, edit, and delete their own posts<br>
&bull; Add and delete their own comments.<br>
A logged in user can add another user as a friend (note that the other user does not have to accept this<br>
friend request, they will become friends immediately). The profile page of a user should indicate<br>
whether they are a friend. If they are a friend then there should be an option to unfriend them.<br>
A link in the navigation menu should list a user's friends.<br>
Posts should have the following privacy settings: <br>
&bull; Public &ndash; seen by non-friends and also if no user is logged in<br>
&bull; Friends &ndash; seen only by friends<br>
&bull; Private &ndash; seen only by the poster<br>
The privacy of a post can be edited at a later date.<br>
Posts have a title and a message and an author (a user). Comments have a message and an author (a<br>
user).<br>
Posts should be displayed on the home page (as per Assignment 1). If a user is not logged in, then the<br>
home page should only display all public posts. If a user is logged in, then the home page should<br>
display all the posts (i.e. public, friends and private) that this user can see. All posts should be<br>
displayed in reverse-order of creation.<br>
Technical requirements<br>
&bull; All user input must be appropriately validated and show validation errors next to the relevant<br>
form field.<br>
&bull; When a post is deleted, all comments must also be deleted, even if the comments are made by<br>
other users.<br>
&bull; Use Laravel&rsquo;s migration for database table creation.<br>
&bull; Use Laravel&rsquo;s seeder to insert default test data into the database. <br>
&bull; Use Laravel&rsquo;s ORM/Eloquent to perform CRUD operations.<br>
&bull; HTML and SQL sanitisation must be performed.<br> <br>
<b>I was not able to complete the following:</b><br>
The profile page will show the user's age not date of birth. <br>
User pic is displayed in friend lists, posts and comments. <br>
Comments should be displayed using pagination with no more than 6 comments per page. <br>
<a href="./home">Back to homepage</a> <br>
</html>



