<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">

    <title>Social Network</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" integrity="sha384-1q8mTJOASx8j1Au+a5WDVnPi2lkFfwwEAa8hDDdjZlpLegxhjVME1fgjWPGmkzs7" crossorigin="anonymous">

    <!-- Optional theme -->
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap-theme.min.css" integrity="sha384-fLW2N01lMqjakBkx3l/M9EahuwpSfeNvV63J5ezn3uZzapT0u7EYsXMjQV+0En5r" crossorigin="anonymous">

    <!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
    <link href="../../assets/css/ie10-viewport-bug-workaround.css" rel="stylesheet">

    <!-- Custom styles for this template -->
   <!-- <link href="./css/styles.css" rel="stylesheet"> -->
    
    <style type="text/css">
      .photo{
    width: 50px;
    height: 50px;
    float: left;
    margin: 5px;
}

.post{
    border-radius: 25px;
    border-style: solid;
    border-color: #ebebeb;
    margin: 10px;
    width: 100%;
    float: left;
}

.container {
    max-width: 900px;
}
    </style>

        <!-- Bootstrap core JavaScript
    ================================================== -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script>window.jQuery || document.write('<script src="../../assets/js/vendor/jquery.min.js"><\/script>')</script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" integrity="sha384-0mSbJDEHialfmuBBQP6A4Qrprq5OVfW37PRR3j5ELqxss1yVqOtnepnHVP9aJ7xS" crossorigin="anonymous"></script>
  </head>

  <body>

    <div class="container">

      <!-- Static navbar -->
      <nav class="navbar navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            {{ link_to_route('home', 'Social Network', array(), array('class' => 'navbar-brand')) }}
          </div>
          <div id="navbar" class="navbar-collapse collapse">
            <ul class="nav navbar-nav navbar-right">
               @if (Auth::check())
              <li>{{ link_to_route('user.logout', 'Logout') }} </li>
               @endif
              <li>{{ link_to_route ('user.index', 'Users')}}</li>
              <li>{{ link_to_route ('doc', 'Documentation')}}</li>
            </ul> 
          </div><!--/.nav-collapse -->
        </div><!--/.container-fluid -->
      </nav>

      <!-- Main body -->
      <div class='row'>
          <div class='col-sm-4'>
              @section('sidebar')
              <ul>
             @if (Auth::check())
                {{ Auth::user()->username }} <br>
                {{ link_to_route('user.showfriends', 'Your friends', array(Auth::user()->id)) }} 
             @else 
                <!-- Login form -->
                {{ Form::open(array('url' => secure_url('user/login'))) }}
                {{ Form::label('username', 'User Name: ') }}
                {{ Form::text('username') }}
                {{ Form::label('password', 'Password: ') }}
                {{ Form::password('password') }}
                {{ Form::submit('Sign in') }}
                {{ Form::close() }}
                {{ link_to_route('user.create', 'Create an Account') }}
             @endif
              </ul> 
              @show
          </div>
          <div class='col-sm-8'>
              @yield('content')
      <!--    </div>    
          <div class='col-sm-0'>
          </div> -->
      </div>

    </div> <!-- /container -->
    
  </body>
</html>