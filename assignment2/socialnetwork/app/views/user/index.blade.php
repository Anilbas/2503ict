@extends('layouts.scaffold')

@section('content')
<div class='list-group-item'>
{{ Form::open(array('method' => 'GET', 'action' => 'UserController@index')) }}
{{ Form::text('search') }} 
{{ Form::submit('Search') }}
</div>

@if ($users)
@foreach ($users as $user)
  <div class='list-group-item'>
    @if ( File::exists(public_path($user->image->url())) )
      <img src="{{ asset($user->image->url('thumb')) }}">
    @else
      <img src="search.png">
    @endif
    {{ link_to_route('user.show', $user->username, $user->id); }}
  </div>
@endforeach
@else
<p>No results.</p>
@endif
@stop 