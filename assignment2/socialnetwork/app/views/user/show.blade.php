@extends('layouts.scaffold')

@section('content')
  <div class='list-group-item'>
    @if ( File::exists(public_path($user->image->url())) )
      <img src="{{ asset($user->image->url('thumb')) }}">
    @else
      <img src="../search.png">
    @endif
    <h4>{{{ $user->username }}}</h4>
    @if ($friends == true)
    <h4> your friend</h4>
    @endif
    Full name: {{{ $user->full_name }}} <br>
    Date of birth: {{{ $user->date_of_birth }}} <br>
    Joined: {{{ $user->created_at }}} <br>  
    {{ link_to_route('user.showfriends', $user->username."'s friends", array($user->id)); }} <br>
    @if (Auth::check() && $me == false)
    @if ($friends == false)
    {{ link_to_route('user.follow', 'Add friend', array('follower' => Auth::user()->username, 'following' => $user->username)) }} 
    @else 
    {{ link_to_route('user.unfollow', 'Remove friend', array('follower' => Auth::user()->username, 'following' => $user->username)) }} 
    @endif  
    @endif 
  </div>
  
  <br> <br> <h4> Posts </h4>
  {{-- show friends posts only if friend, and private posts only if own profile --}}
@foreach ($posts as $post)
{{-- If it is a public post, or (friends post and viewer and page owner are friends), or (it is a private post and viewer and page owner is the same user) --}}
@if ($me == true || ($post->privacy == "public" || ($post->privacy == "friends" && $friends == true))) 
              <div class='list-group-item'>
              <h4>{{{ $post->title }}}</h4>
              Message: {{{ $post->message }}} <br>
              Created: {{{ $post->created_at }}} <br>
              Updated: {{{ $post->updated_at }}} <br>
              Privacy: {{{ $post->privacy }}} <br>
              {{-- Show number of comments - copy from homepage when reanimate --}}
               <?php $zerocomments="true"; ?>
              @foreach ($comments as $comment)
                @if ($comment->id==$post->id)
                {{{ $comment->count}}} 
                  @if ($comment->count==1)
                    comment.
                  @else
                    comments.    
                  @endif
                 <?php $zerocomments="false"; ?>             
                @endif
              @endforeach
              @if ($zerocomments=="true")
                0 comments.
              @endif 
              {{-- show comments - for all posts  --}}              
              {{ link_to_route ('post.show', 'View comments', array($post->id)) }} 
              
      @if ($me == true)
              <br> {{-- editing - only for own posts  --}}
              {{ link_to_route ('post.edit', 'Edit', array($post->id)) }}
              
              {{-- deleting - only for own posts  --}}
              {{ Form::open(array('method' => 'DELETE', 'route' => array('post.destroy', $post->id))) }} 
                  {{ Form::submit('Delete', array('class' => 'btn btn-danger')) }} 
              {{ Form::close() }} 
      @endif
              </div>
@endif  
@endforeach 
  
@stop 