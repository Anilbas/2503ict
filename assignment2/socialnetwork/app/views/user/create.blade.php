@extends('layouts.scaffold')

@section('content')
<div class='list-group-item'>
     {{ Form::open(array('url' => secure_url('user'), 'files' => true))  }}
     {{ Form::label('username', 'User Name: ') }}
     {{ Form::text('username') }} <font color="red">{{ $errors->first('username') }}</font><br>  
     {{ Form::label('password', 'Password: ') }}
     {{ Form::text('password') }} {{-- password type of field? --}} <br> 
     {{ Form::label('full_name', 'Full Name: ') }}
     {{ Form::text('full_name') }} <br>  
     {{ Form::label('date_of_birth', 'Date of birth: ') }}
     {{ Form::text('date_of_birth') }} <br>  
     {{ Form::label('image', 'Userpic:') }}
     {{ Form::file('image') }} <br> 
     {{ Form::submit('Create') }}
     {{ Form::close() }}
</div>
@stop 