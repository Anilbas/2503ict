@extends('layouts.scaffold')

@section('content')
  <div class='list-group-item'>
      <h4>Friends of the user {{ $user->username }}</h4>
  </div>
  @foreach ($friends as $friend)
    <div class='list-group-item'>
        {{  $friend->following }}
    </div>    
  @endforeach
@stop 