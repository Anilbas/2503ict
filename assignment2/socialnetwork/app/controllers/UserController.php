<?php

class UserController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
		$input = Input::all();	
		if (Input::has('search'))
          {
          	$search = $input['search'];
            $users = User::where('username', 'LIKE', '%'.$search.'%')->get();
          } else {
	    	$users = User::all();
          }
		return View::make('user.index', compact('users'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return View::make('user.create');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();
        //validating 
        $v = Validator::make($input, User::$rules);
		if ($v->passes()){
		$password = $input['password'];
        $encrypted = Hash::make($password);
        $user = new User;
        $user->username = $input['username'];
        $user->password = $encrypted;	
        $user->full_name = $input['full_name'];
        $user->date_of_birth = $input['date_of_birth'];
        if (Input::hasFile('image'))
         {
         	$user->image = Input::file('image');
         	
         }
        
      $user->save();
        return  Redirect::route('user.show', array(DB::getPdo()->lastInsertId())); 
		} else {
		 return Redirect::to(url("user/create"))->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
  	    $user = User::find($id);
        $posts = Post::where("user", "=", $user->username)->get();
        if (Auth::check()) {
          $friend = Friend::where("following", "=", $user->username)->where("follower", "=", Auth::user()->username)->first();
          $me = $user->username == Auth::user()->username;
          if ($friend === null) {
             $friends = false;
          } else { $friends = true; }
        } else { $friends = false; $me = false; }
        $sql = "select post_id as id, count(*) as count from comments group by post_id order by post_id";
        $comments = DB::select($sql);
        return View::make('user.show', compact('user', 'posts', 'friends', 'me', 'comments'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
		//
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		//
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
		//
	}
	
	public function login() {
		$username = $_POST['username'];
		$password = $_POST['password'];
		if  (Auth::attempt((compact('username', 'password'))) == true) {
			Session::put('login_error', '');
			return Redirect::to(URL::previous());
		} else {
			Session::put('login_error', 'Login failed');
			return Redirect::to(URL::previous());
		}
	}

    public function logout() {
    	Auth::logout();
			return Redirect::to(URL::previous());
    }
    
    public function follow() {

    	$input = Input::all();
    	$friend = new Friend;
    	$friend->follower = $input['follower'];
    	$friend->following = $input['following']; 
    	try{
       $friend->save(); 
         }
      catch(Exception $e){
       echo $e->getMessage();   
       }
       $friend = new Friend;
    	$friend->follower = $input['following'];
    	$friend->following = $input['follower']; 
       $friend->save(); 
    	return Redirect::to(URL::previous());
    }

    public function unfollow() {
    	$input = Input::all();
    	DB::table('friends')->where('follower', '=', $input['follower'])->where('following', '=', $input['following'])->delete();
    	DB::table('friends')->where('follower', '=', $input['following'])->where('following', '=', $input['follower'])->delete();
      return Redirect::to(URL::previous());
    }
    
    public function showfriends($id) {
    	$user = User::find($id);
    	$friends = DB::table('friends')->where('follower', '=', $user->username)->get();
    	return View::make('user.showfriends', compact('user', 'friends'));
    }

}
