<?php

class PostController extends \BaseController {

	/**
	 * Display a listing of the resource.
	 *
	 * @return Response
	 */
	public function index()
	{
    $posts = Post::all()->sortByDesc("created_at");
    $sql = "select post_id as id, count(*) as count from comments group by post_id order by post_id";
     $comments = DB::select($sql);
		 if (Auth::check()) {
          $friends = Friend::where("follower", "=", Auth::user()->username)->get();
     } else { $friends = array(); }
		return View::make('homepage', compact('posts', 'comments', 'friends'));
	}


	/**
	 * Show the form for creating a new resource.
	 *
	 * @return Response
	 */
	public function create()
	{
		return Redirect::Route('post.index');
	}


	/**
	 * Store a newly created resource in storage.
	 *
	 * @return Response
	 */
	public function store()
	{
		$input = Input::all();	
		$v = Validator::make($input, Post::$rules);
		if ($v->passes()){
		$post = new Post;
		$post->title = $input['title'];
		$post->message = $input['message'];
		$post->user = $input['user'];
	  $post->privacy = $input['privacy'];
		$post->save();
        $id = DB::getPdo()->lastInsertId();
        // If successfully created then display newly created post
        if ($id) 
        {
          return Redirect::to(url("/post"));
        } 
          else
        {
          die("Error adding post");
        }
        
		} else {
		 return Redirect::to(url("/home"))->withErrors($v);
		}
	}


	/**
	 * Display the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function show($id)
	{
	    $post=Post::find($id);
	    $comments = Post::find($id)->comments->sortByDesc("created_at");
  		return View::make('view_comments', compact('post', 'comments'));
	}


	/**
	 * Show the form for editing the specified resource.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function edit($id)
	{
	    $post=Post::find($id);
	  	return View::make('update_post', compact('post'));
	}


	/**
	 * Update the specified resource in storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function update($id)
	{
		$input = Input::all();
		$v = Validator::make($input, Post::$rules);
		if ($v->passes()){
	   $post=Post::find($id);
        $post->title = $input['title'];
        $post->message = $input['message'];
        $post->privacy = $input['privacy'];
        $post->save();
        return Redirect::route('post.show', $post->id);
		} else {
		 return Redirect::to(url("post/$id/edit"))->withErrors($v);
		}
	}


	/**
	 * Remove the specified resource from storage.
	 *
	 * @param  int  $id
	 * @return Response
	 */
	public function destroy($id)
	{
	  $sql = "delete from comments where post_id = ?";
      DB::delete ($sql, array($id));
	  $post = Post::find($id);
      $post->delete();
      return Redirect::route('home');
	}


}
