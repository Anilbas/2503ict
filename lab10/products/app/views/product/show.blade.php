@extends ('product.layout')

@section ('body')
     <b>{{{ $product->name }}}</b><br>
     $ {{{ $product->price }}} <br> <br>
     {{ link_to_route('product.edit', "Edit", array($product->id)) }} <br>
     {{ link_to_route('product.index', "List") }}

@stop