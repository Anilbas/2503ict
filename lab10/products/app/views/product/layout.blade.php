<!doctype html>
<html>
    <head>
        <title> Products </title>
    </head>
    <body>
        <?php 
           $error = Session::get('login_error');        ?>
           <b>{{ $error }}</b>
        @if (Auth::check())
           {{ Auth::user()->username }} {{ link_to_route('user.logout', 'Logout') }} 
        @else 
         <!-- Login form -->
          {{ Form::open(array('url' => secure_url('user/login'))) }}
          {{ Form::label('username', 'User Name: ') }}
          {{ Form::text('username') }}
          {{ Form::label('password', 'Password: ') }}
          {{ Form::password('password') }}
          {{ Form::submit('Sign in') }}
          {{ Form::close() }}
          {{ link_to_route('user.create', 'Create an Account') }}
        @endif
        <br> <br>
@yield ('body')
    </body>