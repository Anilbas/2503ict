 @extends ('product.layout')

@section ('body') 
     Create user <br>
     {{ Form::open(array('url' => secure_url('user')));  }}
     {{ Form::label('username', 'User Name: ') }}
     {{ Form::text('username') }} <font color="red">{{ $errors->first('username') }}</font><br> <br> <br>  
     {{ Form::label('password', 'Password: ') }}
     {{ Form::text('password') }} {{-- password type of field? --}}
     {{ Form::submit('Create') }}
     {{ Form::close(); }}
 @stop 
 
 