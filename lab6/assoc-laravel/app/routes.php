<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

/* Load sample data, an array of associative arrays. */
//require "models/pms.php";


// Display search form
Route::get('/', function()
{
	return View::make('pms.query');
});

// Perform search and display results
Route::get('search', function()
{
  $input = Input::get('input');
  
  $results = search($input);

	return View::make('pms.results')->withPms($results)->withInput($input);
});


/* Functions  */

/* Search sample data for $input from form. */
function search($input) {

  if (!empty($input)) {
    $sql="select * from pms where name like ?";
    $pms1 = DB::select($sql, array("%$input%"));
    $sql="select * from pms where start like ?";
    $pms2 = DB::select($sql, array("%$input%"));
    $sql="select * from pms where finish like ?";
    $pms3 = DB::select($sql, array("%$input%"));
    $sql="select * from pms where state like ?";
    $pms4 = DB::select($sql, array("%$input%"));
    
    if (!empty($pms1)) { $pms=$pms1; }
    if (!empty($pms2)) { $pms=$pms2; }
    if (!empty($pms3)) { $pms=$pms3; }
    if (!empty($pms4)) { $pms=$pms4; }
  } 

  return $pms;

}